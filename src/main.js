import '@babel/polyfill';
import Vue from 'vue';
import './plugins/bootstrap-vue';
import App from './App.vue';
import PayPal from "vue-paypal-checkout";

import Harta from './components/Harta'
Vue.config.productionTip = true
Vue.component('harta', Harta)
Vue.component('paypal', PayPal)

/* eslint-disable no-new */
new Vue({
  el: '#app'
})


Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
